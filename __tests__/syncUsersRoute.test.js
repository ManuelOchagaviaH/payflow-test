const request = require('supertest')
const app = require('../infrastructure/server')

describe('Get Users', () => {
  it('should return OK', async () => {
    const res = await request(app.server)
      .get('/users/sync')

    expect(res.statusCode).toEqual(200)
  })
})