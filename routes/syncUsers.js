const axios = require('axios');
const faker = require('faker');

const syncUsers = (userRepository) => {
  return async (req, res) => {
    let response = await axios.get('https://api.github.com/search/users?q=payflow');
    if (response.status !== 200) res.sendStatus(500);
    else {
      const {
        items
      } = response.data;
      items.forEach((item) => {
        item.email = faker.internet.email();
        item.source = 'github';
        const user = userRepository.findByUserName(item.login);
        const {
          login,
          id,
          avatar_url,
          email,
          source
        } = item;
        if (!user) {
          userRepository.addUser({
            email,
            externalSource: source,
            userName: login,
            externalId: String(id),
            picture: avatar_url,
          });
        } else if (
          user.email !== email ||
          user.externalSource !== externalSource ||
          user.externalId !== id ||
          user.picture !== avatar_url
        ) {
          userRepository.updateUser({
            email,
            externalSource: source,
            userName: login,
            externalId: String(id),
            picture: avatar_url,
          });
        }
      });
      userRepository.deleteNotId(
        items.map((item) => item.login),
        'github'
      );
    }
    res.sendStatus(200);
  };
};

module.exports = syncUsers;